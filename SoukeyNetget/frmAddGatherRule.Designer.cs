﻿namespace SoukeyNetget
{
    partial class frmAddGatherRule
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddGatherRule));
            this.label5 = new System.Windows.Forms.Label();
            this.txtExpression = new System.Windows.Forms.TextBox();
            this.txtRegion = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.comExportLimit = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.comGetType = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.comLimit = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtGetEnd = new System.Windows.Forms.TextBox();
            this.txtGetStart = new System.Windows.Forms.TextBox();
            this.txtGetTitleName = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label5.Name = "label5";
            // 
            // txtExpression
            // 
            resources.ApplyResources(this.txtExpression, "txtExpression");
            this.txtExpression.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtExpression.Name = "txtExpression";
            // 
            // txtRegion
            // 
            resources.ApplyResources(this.txtRegion, "txtRegion");
            this.txtRegion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRegion.Name = "txtRegion";
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.Name = "label37";
            // 
            // comExportLimit
            // 
            resources.ApplyResources(this.comExportLimit, "comExportLimit");
            this.comExportLimit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comExportLimit.FormattingEnabled = true;
            this.comExportLimit.Name = "comExportLimit";
            this.comExportLimit.SelectedIndexChanged += new System.EventHandler(this.comExportLimit_SelectedIndexChanged);
            // 
            // label36
            // 
            resources.ApplyResources(this.label36, "label36");
            this.label36.Name = "label36";
            // 
            // label35
            // 
            resources.ApplyResources(this.label35, "label35");
            this.label35.Name = "label35";
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label28.Name = "label28";
            // 
            // comGetType
            // 
            resources.ApplyResources(this.comGetType, "comGetType");
            this.comGetType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comGetType.FormattingEnabled = true;
            this.comGetType.Name = "comGetType";
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.Name = "label32";
            // 
            // comLimit
            // 
            resources.ApplyResources(this.comLimit, "comLimit");
            this.comLimit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comLimit.FormattingEnabled = true;
            this.comLimit.Name = "comLimit";
            this.comLimit.SelectedIndexChanged += new System.EventHandler(this.comLimit_SelectedIndexChanged);
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // txtGetEnd
            // 
            resources.ApplyResources(this.txtGetEnd, "txtGetEnd");
            this.txtGetEnd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGetEnd.Name = "txtGetEnd";
            // 
            // txtGetStart
            // 
            resources.ApplyResources(this.txtGetStart, "txtGetStart");
            this.txtGetStart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGetStart.Name = "txtGetStart";
            // 
            // txtGetTitleName
            // 
            resources.ApplyResources(this.txtGetTitleName, "txtGetTitleName");
            this.txtGetTitleName.FormattingEnabled = true;
            this.txtGetTitleName.Name = "txtGetTitleName";
            this.txtGetTitleName.SelectedIndexChanged += new System.EventHandler(this.txtGetTitleName_SelectedIndexChanged);
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // button5
            // 
            resources.ApplyResources(this.button5, "button5");
            this.button5.Name = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmAddGatherRule
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtExpression);
            this.Controls.Add(this.txtRegion);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.comExportLimit);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.comGetType);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.comLimit);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtGetEnd);
            this.Controls.Add(this.txtGetStart);
            this.Controls.Add(this.txtGetTitleName);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAddGatherRule";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmAddGatherRule_FormClosed);
            this.Load += new System.EventHandler(this.frmAddGatherRule_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtExpression;
        private System.Windows.Forms.TextBox txtRegion;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox comExportLimit;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox comGetType;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox comLimit;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtGetEnd;
        private System.Windows.Forms.TextBox txtGetStart;
        private System.Windows.Forms.ComboBox txtGetTitleName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button1;
    }
}